# Rooster - The Shift Manager #
 
## Introduction ##

 
Today's world operate in shift to support people across the globe. IT teams work round the clock to ensure that their operations operate smooth. A Hospital has to be available 24x7 to save lives. Hotels, restaurant, airports, retails are a few other services that remind us of the need to work in shift. Shifts have become an integral part of our work.

Most of the shifts are still managed in sheets. On a weekly/monthly or quarterly basis, supervisor has to work on rotating the sheets. Owners, managers or supervisor need a tool that helps them understand whether the shift is operating in its full strength or not. They don't have an application that helps evaluate whether their manpower meets the demand.

Shift employees should be able to communicate with each other to swap shifts, in case of last minute emergency. When your workforce swap shift, supervisor should be able to identify and keep track of it.
Rooster will be a shift manager application which is built as a web app with mobile-first approach, which will enable users on the go to use their mobile apps to update status and use the web app for generating reports and major initial configurations.

## Basic features: ##

* Roster generator for a group of technicians, distributed upon selected shifts and rotation period.
* Ability to build a shift easily by a team leader: Based on available personnel, no of hours per week and no days per week.
* Ability to exchange shifts within personnel with/without lead approval
* Ability to generate shift manually
* Daily agenda, to show who are working today, who is not working today, who is absent.
* Home Dashboard to show daily agenda for today, tomorrow and the day after.
* User specific logins having user-specific views for calendars and dashboard customizations.


## Market Analysis ##

Although the introduction portrays the shift management market akin to stone age, there are many competitors in this space. Below is an analysis of a few promising products in the market.

### ShiftNote ###
website: https://www.shiftnote.com/
pricing: https://www.shiftnote.com/pricing1/ (free 30 day trial)
 
* Cloud based. Launched in 2002 with a focus on hospitality.
 
* They provide two distinct editions: Logbook & Scheduler. Scheduler having the superset of features.
 
* Fairly feature rich, list of features and differences between Logbook and Scheduler is present in the pricing page.
 
* Scheduler has got announcements, saving schedule details as templates, picking up shifts when needed and releasing shifts when can't work, inbuilt approval processes, reports about hours worked, time-offs and shift trades, Agenda and calendar views.
 
* Logbook is a note-tracking tool for teams who need to continue the work of a previous shift employee, and also many other features.
 
* Mobile apps available.
 
* ShiftNote Scheduler overview: https://vimeo.com/64662569
 
* ShiftNote Logbook overview: https://vimeo.com/56339315

 
### ShiftPlanning ###
website: https://www.shiftplanning.com/
pricing: https://www.shiftplanning.com/signup (free 30 day trial)
 
* Cloud based. open since 2009.

* Includes Scheduling, TimeClock, Human Resources and Payroll. Maybe complex for some users but overall value for money as it includes a lot of components.

* Each module scope is as large as a separate tool but in this software working together seamlessly.

* USPs of Scheduler: Drag and drop calendar interface, Conflict free schedules, Calendar sync with google calendar, iCal & outlook.

* USPs of TimeClock: Real time available personnel, biometric verification of attendance, Location verification using IP, Timeclock terminals can be setup like a POS

* USPs of Payroll: Integrates with scheduler and timeclock, custom pay rates, easy reports.

* USPs of HR Software: Vacation tracking and leave mgmt, Learning and document mgmt, Karma score based on attendance, FB integration, FB Wall-like page for communication and to know who's working now (team specific), Private Messaging, Dashboards, etc.,

* It is now called as Humanity, but for some reason operates both as separate products.

* Humanity offers some integrations with other tools: Integrations List

* Humanity short overview: https://youtu.be/dcQ2aqW_qAs

* full demo: https://www.youtube.com/watch?v=GpmNyriFSO0

* short overview: https://www.youtube.com/watch?v=gRn_FESSTU0

 

### Shiftboard ###
website: https://www.shiftboard.com/
pricing: https://www.shiftboard.com/standard-pricing/
 
* Cloud based. Launched in 2002 with a focus on healthcare industry.

* Similar to ShiftPlanning, Shiftboard offers HR, Time sheet, Shift Scheduling and Communication tools.

* Couldn't find an offcial overview video: https://youtu.be/B62goDeFXl8

 
### Sling ###
website: https://getsling.com/
pricing: free
 
* Cloud based, Icelandic company.

* Provides basic functionality like shift scheduling, Adding tasks, Managing team members, etc.

* It has a newsfeed feature for notification display and communication.

* There is also messaging functionality for direct private messages and group messages.

* short overview: https://youtu.be/cdkQlI0Sov0

 
### ((sirenum)) ###
website: http://sirenum.com/
pricing: NA
 
* Founded in 2013.

* They manage HRMS, scheduling of shifts, monitoring attendance and time to work, payroll and reporting functionality

* They have mobile apps to complement their web products.

* There are no real details about the pricing, mode of deployment, or any reliable screenshot.

 
### Deputy ###
website: https://www.deputy.com/
pricing: https://www.deputy.com/pricing (free 30 days trial)
 
* Cloud based, founded in 2008

* You can manage employee scheduling for shifts, attendance and time keeping, Task delegation and communication using chat.

* Deputy offers a lot of add-ons for integrating with your favourite payroll and HRMS applications. Here a list: https://www.deputy.com/add-ons

## Summary and Findings ##

The products lack integrations with other HR tools out of the box or offer a HR tool of their own. We can focus on HR tools with a large user base and provide integrations to tap into their customer base.
 
Many products have a vertical oriented approach, we should be able to configure our application to suit various verticals like hospitality, healthcare, helpdesk, call centre, manufacturing, IT ops, etc., with specific but different terms for the same functionality and roles. We should offer this out of the box and  This will make lead conversion faster as customers don't need to figure out how this will fit into their vertical specific roles and shifts.
 
Goal is to be small and provide best of integrations with existing payroll and HRMS providers, as most of the companies would have HRMS and payroll but think shift management is optional or easy enough to be handled via excel sheets. They look for Shift Management tools when their process becomes unmanageable so most would not need payroll or HRMS but would definitely need shift management. We would have to build our own or recomment a payroll/HRMS tool for our customer and tie up with that product.
 
## References: ##

http://www.capterra.com/employee-scheduling-software/

http://thegeekdesire.com/best-free-employee-scheduling-software.html

https://www.getapp.com/hr-employee-management-software/employee-scheduling/#getrank