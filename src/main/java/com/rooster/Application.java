package com.rooster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by alan.francis on 04/11/16.
 *
 */
@SpringBootApplication
public class Application {

	public static void main(String args[]){
		SpringApplication.run(Application.class, args);
	}
}
